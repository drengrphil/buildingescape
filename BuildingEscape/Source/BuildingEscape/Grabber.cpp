// Copyright PanGroove Studio 2020
// #include "CollisionQueryParams.h"
#include "Grabber.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"

#define OUT

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	FindPhysicsHandle();
	SetupInputComponent();
}

void UGrabber::FindPhysicsHandle()
{
	// Checking for Physics handle component
	PhysicsHandle  = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (PhysicsHandle == nullptr) // Or (!PhysicsHandle)
	{
		UE_LOG(LogTemp, Error, TEXT("No physics handle found on %s!"), *GetOwner()->GetName());
	}
}

void UGrabber::SetupInputComponent()
{
	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
	if (InputComponent)
	{
		// UE_LOG(LogTemp, Warning, TEXT("Input Component Found %s"), *GetOwner()->GetName());
		// Bind action to the input keys.
		InputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		InputComponent->BindAction("Grab", IE_Released, this, &UGrabber::Release);
	}
}

// User input map to this function 
void UGrabber::Grab()
{
	// Ray cast only when its time to pick up something.
	FHitResult HitResult = GetFirstPhysicsBodyInReach();
	UPrimitiveComponent* ComponentToGrab = HitResult.GetComponent();
	AActor* ActorHit = HitResult.GetActor();
	// Reach any actors with physics body collision channel set.
	// If we hit something then attached the physics handle.
	// TODO attach physics handle.
	if (ActorHit)
	{
		if (!PhysicsHandle) {return;}
		PhysicsHandle->GrabComponentAtLocation
		(
			ComponentToGrab, // Component to grab
			NAME_None,
			GetPlayerReachEnd()
		);
	}
}


void UGrabber::Release()
{
	if (!PhysicsHandle) {return;}
	PhysicsHandle->ReleaseComponent();
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	// If the physics handle is attach.
	    // Move the object we are holding.
	if (!PhysicsHandle) {return;}
	if (PhysicsHandle->GrabbedComponent)
	{
		// Something is grabbed. Now set target location to move grabbed object to.
		PhysicsHandle->SetTargetLocation(GetPlayerReachEnd());
	}
}

FHitResult UGrabber::GetFirstPhysicsBodyInReach() const
{

	FHitResult Hit;
	FCollisionQueryParams TraceParams(FName(TEXT("")), false, GetOwner()); // GetOwner helps us ignore ourselves because grabber is on the player
	GetWorld()->LineTraceSingleByObjectType(
		OUT Hit,
		GetPlayerWorldPosition(),
		GetPlayerReachEnd(),
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		TraceParams
	);
	return Hit;
}

FVector UGrabber::GetPlayerWorldPosition() const
{
	// Get the player's viewpoint
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPointLocation, 
		OUT PlayerViewPointRotation
	);

	// Ray-cast out to a certain distance (reach)
	return PlayerViewPointLocation;

}

FVector UGrabber::GetPlayerReachEnd() const
{
	// Get the player's viewpoint
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPointLocation, 
		OUT PlayerViewPointRotation
	);

	// Ray-cast out to a certain distance (reach)
	return  (PlayerViewPointLocation + PlayerViewPointRotation.Vector() * Reach);
}

// // Draw a line from player showing the reach.
// Main helps as debug
// FVector LineTraceEnd = PlayerViewPointLocation + PlayerViewPointRotation.Vector() * Reach;
// //FVector(0.f, 0.f, 100.f);
// DrawDebugLine(
// 	GetWorld(),
// 	PlayerViewPointLocation,
// 	LineTraceEnd,
// 	FColor(0, 255, 0),
// 	false,
// 	0.f,
// 	0,
// 	5.f
// );
	// // See what it hits
	// AActor* ActorHit = Hit.GetActor();
	// if (ActorHit)
	// {
	// 	UE_LOG(LogTemp, Warning, TEXT("Line trace has hit: %s"), *(ActorHit->GetName()));
	// }