// Fill out your copyright notice in the Description page of Project Settings.

#include "WorldPosition.h"
#include "GameFramework/Actor.h"

// Sets default values for this component's properties
UWorldPosition::UWorldPosition()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UWorldPosition::BeginPlay()
{
	Super::BeginPlay();

	FString ObjectName = GetOwner()->GetName();
	UE_LOG(LogTemp, Warning, TEXT("Object Name is: %s"), *GetOwner()->GetName());
	// FString ObjectPosition = GetOwner()->GetActorLocation().ToString();
	// We could use GetActorTransform() as well.
	FString ObjectPosition = GetOwner()->GetActorTransform().GetLocation().ToString();
	UE_LOG(LogTemp, Warning, TEXT("%s position in world is: %s"), *ObjectName, *ObjectPosition);
	/*FString Log = TEXT("Hello!");
	FString* PtrLog = &Log;  // Pointer to memory address of Log.
	Log.Len();
	// Get value stored in the addr
	(*PtrLog).Len(); // Dereference
	PtrLog->Len(); // Using Pointer

	// UE Logging
	UE_LOG(LogTemp, Warning, TEXT("%s"), *Log);
	UE_LOG(LogTemp, Warning, TEXT("%s"), **PtrLog);*/
	
}


// Called every frame
void UWorldPosition::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

